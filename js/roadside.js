class Roadside {
    constructor(scene) {
        const box = BABYLON.Mesh.CreateBox('box1', 3, scene);
        box.material = new BABYLON.StandardMaterial("texture1", scene);
        box.material.emissiveColor = new BABYLON.Color3(1, 1, 0);
        box.ellipsoid = new BABYLON.Vector3(1, 1, 1);

        this.box = box;
    }

    applyPhysics() {
        this.box.physicsImpostor = new BABYLON.PhysicsImpostor(this.box, BABYLON.PhysicsImpostor.BoxImpostor, {
            mass: 0,
            restitution: 0,
            friction: 0,
        }, this._scene);
    }
}
