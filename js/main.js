function main() {
    var canvas = document.getElementById('renderCanvas');
    var engine = new BABYLON.Engine(canvas, true);
    var scene = new BABYLON.Scene(engine);

    //creating the components for the physics engine
    var gravityVector = new BABYLON.Vector3(0, -9.81, 0);
    var physicsPlugin = new BABYLON.CannonJSPlugin();
    scene.enablePhysics(gravityVector, physicsPlugin);

    //creating camera, light, a ground, a car and a roadside
    var camera = new Camera(scene);
    var light = new Light(scene);
    var ground = new Ground(scene);
    var car = new Car(scene);
    var map = new Map(scene);

    //applying the physics to the objects
    ground.applyPhysics();
    car.applyPhysics();
    map.applyPhysics();

    //initializing the camera (especially the target)
    camera.updateCamera(car.direction, car.mesh.position, keys, car.target);

    //array for the keys which are pressed in the game, and the events that are triggered when they're pressed
    var keys = [];
    document.onkeydown = (e) => keys[e.keyCode] = true;
    document.onkeyup = (e) => keys[e.keyCode] = false;


    engine.runRenderLoop(function() {
        scene.render();
    });

    //the following functions are called every frame
    scene.registerBeforeRender(function() {
        car.updateMovement(keys, camera);
        camera.updateCamera(car.direction, car.mesh.position, keys, car.target);
        light.updateLight(car.target.position);

        window.onresize = () => engine.resize();
    });

}
window.onload = main;
