class Map{
  constructor(scene){
    this.roadside = [
      new Roadside(scene),
      new Roadside(scene),
      new Roadside(scene),
      new Roadside(scene),
      new Roadside(scene)
    ];

    this.roadside[0].box.position = new BABYLON.Vector3(0, 1, 50);
    this.roadside[1].box.position = new BABYLON.Vector3(0, 1, -50);
    this.roadside[2].box.position = new BABYLON.Vector3(0, 1, 150);
    this.roadside[3].box.position = new BABYLON.Vector3(150, 1, 50);
    this.roadside[4].box.position = new BABYLON.Vector3(-150, 1, 50);

    this.roadside[0].box.scaling.x = 40;
    this.roadside[1].box.scaling.x = 100;
    this.roadside[2].box.scaling.x = 100;

    this.roadside[0].box.scaling.z = 20;
    this.roadside[3].box.scaling.z = 70;
    this.roadside[4].box.scaling.z = 70;
  }

  applyPhysics(){
    this.roadside.forEach(roadside => roadside.applyPhysics());
  }
}
